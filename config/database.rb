require "rubygems"
require "dm-core"
require "dm-validations"
require './app/models/snapshot'
require './app/models/stock'

DataMapper.setup(
  :default,
  ENV['DATABASE_URL'] || "sqlite3://#{Dir.pwd}/config/development.db"
)

DataMapper::Model.raise_on_save_failure = false
DataMapper.finalize