# Contains functionality to scrape financial data from finviz
require "csv"
require "open-uri"
require "awesome_print"
require "json"

module Tools

  class FinvizScraper

    def initialize(url)
      @url = url
    end

    # Initial seed data from finviz.com
    def import_data(snapshot)

      puts 'Importing stocks...'

      CSV.new(open(@url), :headers => :first_row).each do |row|

        puts "#{row['Ticker']} -- #{row['Company']}"

        stock = Finance::Stock.new
        stock.ticker = row['Ticker']
        stock.company = row['Company']
        stock.sector = row['Sector']
        stock.industry = row['Industry']
        stock.marketcap = row['Market Cap']
        stock.price_earnings = row['P/E']
        stock.price_sales = row['P/S']
        stock.price_book = row['P/B']
        stock.price_freecashflow = row['P/Free Cash Flow']
        stock.price_cash = row['P/Cash']
        stock.price_earnings_growth = row['PEG']
        stock.dividend_yield = row['Dividend Yield']
        stock.total_debt = row['Total Debt/Equity']
        stock.performance_half_year = row['Performance (Half Year)']
        stock.price = row['Price']
        stock.snapshot_id = snapshot.id

        stock.EV_EBITDA = _fetch_ev_ebitda(stock.ticker)
        stock.buy_back = _fetch_buy_back(stock.ticker)

        stock.buy_back_yield = _calculate_buy_back_yield(stock.marketcap, stock.buy_back)
        stock.shareholder_yield = _calculate_shareholder_yield(stock.dividend_yield, stock.buy_back_yield)

        if stock.save
          # success do nothing
        else
          stock.errors.each do |e|
            puts e
          end
        end

        snapshot.stocks << stock
      end

      snapshot.save
    end

    private

    # Retrieve Buy Back from YQL Open Data Table
    def _fetch_buy_back(symbol)

      json = Utils.Yql(%{
        use 'store://atO0MfCG2W48ffggHJfsZ4' as cashflow;
        select * from cashflow where symbol = '#{symbol}'
      })

      response = json['query']['results']['cashflow']
      value = 0

      statement_object = response['statement']
      if !response.nil? and !statement_object.nil?

        ap response if symbol=="AAV"
        if !statement_object.empty? and statement_object.is_a?(Array)
          statement_object.each do |statement|
            object = statement['SalePurchaseofStock']['content']
            value += object.to_f if object != '-'
          end
        else
          value += statement_object['SalePurchaseofStock']['content'].to_f
        end
      end

      value
    end

    # Retrieve EV/EBITDA from YQL Data table
    def _fetch_ev_ebitda(symbol)
      json = Utils.Yql(%{
        select * from yahoo.finance.keystats where symbol = '#{symbol}'
      })

      response = json['query']['results']['stats']['EnterpriseValueEBITDA']

      value = 9999999.99
      if !response.nil? and response['content'] != 'N/A'
        value = response['content'].to_f
      end

      value
    end

    def _calculate_buy_back_yield(market_cap, buy_back)
      (buy_back *-1) / (market_cap * 1000000) * 100
    end

    def _calculate_shareholder_yield(dividend_yield, buy_back_yield)
      dividend_yield.to_f + buy_back_yield.to_f
    end

  end

end