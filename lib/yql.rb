require 'net/http'
require 'rubygems'
require 'json'
 
module Utils
  def self.Yql(query)
    uri = "http://query.yahooapis.com/v1/public/yql"
   
    # everything's requested via POST, which is all I needed when I wrote this
    # likewise, everything coming back is json encoded
    response = Net::HTTP.post_form( URI.parse( uri ), {
      'q' => query,
      'format' => 'json',
      'env' => 'http://www.datatables.org/alltables.env'
    } )
   
    json = JSON.parse( response.body )
    return json
  end
end