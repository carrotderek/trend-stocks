module Finance

  class Stock
    include DataMapper::Resource

    belongs_to :snapshot

    property :id,                     Serial
    property :ticker,                 String
    property :company,                String
    property :sector,                 String
    property :industry,               String
    property :marketcap,              Float
    property :price_earnings,         Float
    property :price_sales,            Float
    property :price_book,             Float
    property :price_freecashflow,     Float
    property :price_cash,             Float
    property :price_earnings_growth,  Float
    property :dividend_yield,         String
    property :performance_half_year,  String
    property :total_debt,             Float  
    property :price,                  Float
    property :buy_back,               Float
    property :EV_EBITDA,              Float


    property :buy_back_yield,         Float
    property :shareholder_yield,      Float

    property :price_earnings_rank,    Float
    property :price_sales_rank,       Float
    property :price_book_rank,        Float
    property :price_earnings_rank,    Float
    property :price_freecashflow_rank,Float
    property :shareholder_yield_rank, Float
    property :EV_EBITDA_rank,         Float
    property :rank,                   Float
    property :overall_rank,           Float
  end

end