module Finance

  class Snapshot
    include DataMapper::Resource

    has n, :stocks

    property :id, Serial
    property :created_at, DateTime
    property :completed, Boolean, :default => false
  end

end